import chalk from 'chalk';

/**
 * Logs provided arguments to the console.
 * - If an argument is a string, it's directly printed.
 * - If an argument is an object and has a property starting with a dot, 
 *   it treats that property's key (without the dot) as a chalk color and the value as the string to color.
 *   If no such property exists, the object itself is returned.
 * - Other types are converted to string and printed.
 *
 * @param {Array<any>|any} args The arguments to be logged.
 * @returns {Promise<void>}
 */
export default async (args) => {
    if (Array.isArray(args)) {
        const processedArgs = args.map(arg => {
            if (typeof arg === 'string') {
                return arg;
            } else if (typeof arg === 'object' && !Array.isArray(arg)) {
                // Try to get the first property starting with a dot.
                const [key, value] = Object.entries(arg).find(([k]) => k.startsWith('.')) || [];

                if (key && chalk[key.substring(1)]) {
                    return chalk[key.substring(1)](value);
                } else {
                    return arg;  // If no color code property exists, return the object itself.
                }
            } else {
                return arg.toString(); // Convert other types to string and return.
            }
        });

        console.log(...processedArgs);
    } else {
        console.log(args);
    }
};